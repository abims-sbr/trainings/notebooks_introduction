# 🎓 Introduction to notebooks

- Slides: [abims-sbr.gitlab.io/trainings/notebooks_introduction/index.html](https://abims-sbr.gitlab.io/trainings/notebooks_introduction/index.html)
- Tutorial: [abims-sbr.gitlab.io/trainings/notebooks_introduction/tutorial.html](https://abims-sbr.gitlab.io/trainings/notebooks_introduction/tutorial.html)

